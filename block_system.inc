#if defined block_system_included
	#endinput
#endif
#define block_system_included

/*
	global vars
*/

#if defined mysql_included
	static MySQL: g_block_system_sql_handle;
	static Cache: g_block_system_load_info_cache[MAX_PLAYERS] = { Cache:-1, Cache:-1,  Cache:-1, ... };
#elseif defined sql_open
	static SQL: g_block_system_sql_handle;
	static Result: g_block_system_load_info_cache[MAX_PLAYERS] = { Result:-1, Result:-1, Result:-1, ... };
#endif

static bool: g_is_player_block[MAX_PLAYERS] = { false, false, ... };


/*
	hook callbaks
*/


public OnPlayerDisconnect(playerid, reason)
{
	#if defined mysql_included
		new Cache: cache_id = g_block_system_load_info_cache[playerid];
		if (cache_is_valid(cache_id))
		{
			cache_delete(cache_id);
			g_block_system_load_info_cache[playerid] = Cache:-1;
		}
	#elseif defined sql_open
		new Result: cache_id = g_block_system_load_info_cache[playerid];
		if (cache_id != Result:-1)
		{
			sql_free_result(cache_id);
			g_block_system_load_info_cache[playerid] = Result:-1;
		}
	#endif

	#if defined block_system_OnPlayerDisconnect
		return block_system_OnPlayerDisconnect(playerid, reason);
	#else
		return 1;
	#endif
}
#if defined _ALS_OnPlayerDisconnect
	#undef OnPlayerDisconnect
#else
	#define _ALS_OnPlayerDisconnect
#endif

#define OnPlayerDisconnect block_system_OnPlayerDisconnect
#if defined block_system_OnPlayerDisconnect
	forward block_system_OnPlayerDisconnect(playerid, reason);
#endif


#if !defined _mdialog_included && !defined _TDW_DIALOG_INC_

	/*
		hook OnDialogResponse
	*/

	public OnDialogResponse(playerid, dialogid, response, listitem, inputtext[])
	{
		return 1;
	}
	#if defined _ALS_OnDialogResponse
		#undef OnDialogResponse
	#else
		#define _ALS_OnDialogResponse
	#endif

	#define OnDialogResponse block_system_OnDialogResponse
	#if defined block_system_OnDialogResponse
		forward block_system_OnDialogResponse(playerid, dialogid, response, listitem, inputtext[]);
	#endif

	static stock ShowPlayerDialogBanInfo(const playerid)
	{
		static buffer[256]; // you need to put the cock on the memory, it's not so important. it's better to chase CPU time
		GetDialogTextBanInfo(playerid, buffer);
		printf("buffer: %s", buffer);
		ShowPlayerDialog(
			playerid, 
			DIALOG_BAN_INFO, 
			DIALOG_STYLE_MSGBOX, 
			"{00bfff}Вы получили бан", 
			buffer, 
			"Окей",
			""
		);
		buffer[0] = '\0';
		return 1;
	}
#endif


/*
	MySQL callbacks
*/

#if defined mysql_included
	forward OnPlayerLoadBans(const Cache: cache_id, const playerid);
	public OnPlayerLoadBans(const Cache: cache_id, const playerid)
#elseif defined sql_open
	forward OnPlayerLoadBans(const Result: res, const playerid);
	public OnPlayerLoadBans(const Result: res, const playerid)
#endif
{
	#if defined mysql_included
		g_block_system_load_info_cache[playerid] = cache_id;
	#elseif defined sql_open
		g_block_system_load_info_cache[playerid] = res;
		sql_store_result(res);
	#endif


	#if defined _TDW_DIALOG_INC_
		Dialog_Show(playerid, "DialogPlayerPlayerDialogBanInfo");
	#elseif defined _mdialog_included 
		Dialog_Show(playerid, Dialog:DialogPlayerPlayerDialogBanInfo);
	#else
		ShowPlayerDialogBanInfo(playerid);
	#endif
}

#if defined mysql_included 
	forward OnLoadBlockIpAddreas();
	public OnLoadBlockIpAddreas()
#elseif defined sql_open
	forward OnLoadBlockIpAddreas(const Result: res);
	public OnLoadBlockIpAddreas(const Result: res)
#endif
{
	new 
		ip_address[16],
		unban_time = 0;
	#if defined mysql_included
		new rows = cache_num_rows();
		for (new i = 0; i < rows; i++)
		{
			cache_get_value_name(i, "ip_address", ip_address);
			cache_get_value_name_int(i, "ms_unban", unban_time);

			BlockIpAddress(ip_address, unban_time);
			ip_address[0] = '\0';
		}
	#elseif  defined sql_open
		new rows = sql_num_rows(res);
		for (new i = 0; i < rows; i++)
		{
			unban_time =  sql_get_field_assoc_int(res, "ms_unban");
			sql_get_field_assoc(res, "ip_address", ip_address);

			BlockIpAddress(ip_address, unban_time);
			
			ip_address[0] = '\0';
			sql_next_row(res);
		}
		sql_free_result(res);
	#endif
}

/*
	function
*/

#if defined mysql_included
	stock block_system_SetMySQLHandle(const MySQL: handle)
#elseif defined sql_open
	stock block_system_SetMySQLHandle(const SQL:handle)
#endif
{
	g_block_system_sql_handle = handle;
	return 1;
}

stock bool: block_system_IsPlayerBlock(const playerid)
{
	return g_is_player_block[playerid];
}

stock block_system_LoadIpAddressBan()
{
	static query[] = "SELECT \
		INET_NTOA(ip_address) as ip_address, \
		timestampdiff(SECOND, NOW(), date_unban) * 1000 AS ms_unban \
	FROM "TABLE_USERS_BAN" WHERE `is_block_ip` = '1'";

	#if defined mysql_included
		mysql_tquery(
			g_block_system_sql_handle, 
			query, 
			.callback = "OnLoadBlockIpAddreas", 
			.format = ""
		);
	#elseif defined sql_open
		sql_query(
			g_block_system_sql_handle, 
			query, 
			.flag = QUERY_NONE, 
			.callback = "OnLoadBlockIpAddreas", 
			.format = "r"
		);
	#endif 
	return 1;
}

#if defined _TDW_DIALOG_INC_
	dtempl DialogPlayerPlayerDialogBanInfo(playerid)
	{
		static buffer[256]; // you need to put the cock on the memory, it's not so important. it's better to chase CPU time
		GetDialogTextBanInfo(playerid, buffer);
		Dialog_Open(
			playerid, 
			dfunc:DialogPlayerPlayerDialogBanInfo, 
			DIALOG_STYLE_MSGBOX, 
			"{00bfff}Вы получили бан", 
			buffer, 
			"пїЅпїЅ", 
			""
		);
		return 1;
	}
#elseif defined _mdialog_included
	DialogCreate:DialogPlayerPlayerDialogBanInfo(playerid)
	{
		static buffer[256]; // you need to put the cock on the memory, it's not so important. it's better to chase CPU time
		GetDialogTextBanInfo(playerid, buffer);
		Dialog_Open(
			playerid, 
			Dialog:DialogPlayerPlayerDialogBanInfo, 
			DIALOG_STYLE_MSGBOX,
			"{00bfff}Вы получили бан", 
			buffer
			"Ок", 
			""
		);
		return 1;
	}
#endif

/*
	dialog helpers
*/

static stock GetDialogTextBanInfo(const playerid, dialog_text[], const size = sizeof(dialog_text))
{
	static const dlg_text[] = "{ffffff}Это аккаунт заблокирован на %d %s\n\n\
	Ник администратора: %s\n\
	Причина блокировки: %s\n\
	Дата и время: %s";

	new	
		days_to_unban,
		adm_name[MAX_PLAYER_NAME + 1],
		reason[MAX_CHATBUBBLE_LENGTH],
		date_banned[64],
		declension[128];

	
	#if defined mysql_included
		new 
			Cache: cache_id = g_block_system_load_info_cache[playerid];

		if (!cache_is_valid(cache_id))
		{
			return 0;
		}

		cache_set_active(cache_id);
		new rows = cache_num_rows();

		if (rows == 0)
		{
			return 0;
		}

		cache_get_value_name_int(0, "days_unban", days_to_unban);
		cache_get_value_name(0, "adm_user_name", adm_name);
		cache_get_value_name(0, "reason", reason);
		cache_get_value_name(0, "date_banned", date_banned);

		cache_unset_active();
		cache_delete(cache_id);
		g_block_system_load_info_cache[playerid] = Cache:-1;
	#elseif defined sql_open
		new 
			Result: cache_id = g_block_system_load_info_cache[playerid];
		
		if (cache_id == Result:-1)
		{
			return 0;
		}
			
		new rows = sql_num_rows(cache_id);

		printf("GetDialogTextBanInfo | rows: %d", rows);
		if (rows == 0)
		{
			return 0;
		}

		days_to_unban = sql_get_field_assoc_int(cache_id, "days_unban");
		sql_get_field_assoc(cache_id, "adm_user_name", adm_name);
		sql_get_field_assoc(cache_id, "reason", reason);
		sql_get_field_assoc(cache_id, "date_banned", date_banned);

		sql_free_result(cache_id);
		g_block_system_load_info_cache[playerid] = Result: -1;
	#endif

	utils_GetDeclensionWord(
		declension, 
		sizeof(declension), 
		days_to_unban,
		"день",
		"дня",
		"дней"
	);
	format(
		dialog_text, 
		size, 
		dlg_text,
		days_to_unban,
		declension,
		adm_name,
		reason,
		date_banned
	);
	g_is_player_block[playerid] = true;
	return 1;
}