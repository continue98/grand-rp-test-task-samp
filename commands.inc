#if defined _cmd_included
	#endinput
#endif
#define _cmd_included


CMD:ban(playerid, params[])
{
	new 
		targetid,
		count_days,
		reason[MAX_CHATBUBBLE_LENGTH],
		is_block_ip;

	if (sscanf(params, "ddS()[144]D(0)", targetid, count_days, reason, is_block_ip))
	{
		SendClientMessage(playerid, -1, "Используйте: /ban [ид игрока][количество дней (1-30)]{причина}{забанить ип}");
		return 0;
	}

	if (IsPlayerConnected(targetid) == 0)
	{
		SendClientMessage(playerid, -1, "Игрок не подключен!");
		return 0;
	}

	
	#if defined mysql_included	
		if (reason[0] == '\0' && is_block_ip == 0)
		{
			static const query[] = "INSERT INTO "TABLE_USERS_BAN" \
			(`user_id`, `ip_address`, `adm_id`, `date_banned`, `date_unban`) VALUES \
			('%d', INET_ATON('%e'), '%d', NOW(), NOW() + INTERVAL %d DAY)";
			new fmt_query[sizeof(query) - 2 + 16 + 11 * 3];
			mysql_format(
				g_sql_connect, 
				fmt_query,
				sizeof(fmt_query), 
				query, 
				g_player_info[targetid][player_account_id],
				g_player_info[targetid][player_ip_address],
				g_player_info[playerid][player_account_id],
				count_days
			);
			mysql_tquery(
				g_sql_connect, 
				fmt_query,
				.callback = "", 
				.format = ""
			);
		}
		else if (reason[0] != '\0' && is_block_ip == 1)
		{
			static const query[] = "INSERT INTO "TABLE_USERS_BAN" \
			(`user_id`, `ip_address`, `adm_id`, `date_banned`, `date_unban`, `is_block_ip`, `reason`) VALUES \
			('%d', INET_ATON('%e'), '%d', NOW(), NOW() + INTERVAL %d DAY, '%d', '%e')";
			new fmt_query[sizeof(query) - 2 * 6 + 11 * 4 + 64 + 16 + MAX_CHATBUBBLE_LENGTH];
			mysql_format(
				g_sql_connect, 
				fmt_query,
				sizeof(fmt_query), 
				query, 
				g_player_info[targetid][player_account_id],
				g_player_info[targetid][player_ip_address],
				reason,
				g_player_info[playerid][player_account_id],
				count_days,
				is_block_ip,
				reason
			);
			mysql_tquery(
				g_sql_connect, 
				fmt_query,
				.callback = "", 
				.format = ""
			);			
		}
		else if (reason[0] != '\0')
		{
			static const query[] = "INSERT INTO "TABLE_USERS_BAN" \
			(`user_id`, `ip_address`, `adm_id`, `date_banned`, `date_unban`, `reason`) VALUES \
			('%d', INET_ATON('%e'), '%d', NOW(), NOW() + INTERVAL %d DAY, '%e')";

			new fmt_query[sizeof(query) - 2 * 6 + 11 * 4 + 64 + 16 + MAX_CHATBUBBLE_LENGTH];
			mysql_format(
				g_sql_connect, 
				fmt_query,
				sizeof(fmt_query), 
				query, 
				g_player_info[targetid][player_account_id],
				g_player_info[targetid][player_ip_address],
				reason,
				g_player_info[playerid][player_account_id],
				count_days,
				reason
			);
			mysql_tquery(
				g_sql_connect, 
				fmt_query,
				.callback = "", 
				.format = ""
			);
		}
		else if (is_block_ip == 1)
		{
			static const query[] = "INSERT INTO "TABLE_USERS_BAN" \
			(`user_id`, `ip_address`, `adm_id`, `date_banned`, `date_unban`, `is_block_ip`) VALUES \
			('%d', INET_ATON('%e'), '%d', NOW(), NOW() + INTERVAL %d DAY, '%d')";
			new fmt_query[sizeof(query) - 2 * 6 + 11 * 4 + 64 + 16 + MAX_CHATBUBBLE_LENGTH];
			mysql_format(
				g_sql_connect, 
				fmt_query,
				sizeof(fmt_query), 
				query, 
				g_player_info[targetid][player_account_id],
				g_player_info[targetid][player_ip_address],
				reason,
				g_player_info[playerid][player_account_id],
				count_days,
				is_block_ip
			);
			mysql_tquery(
				g_sql_connect, 
				fmt_query,
				.callback = "", 
				.format = ""
			);
		}
		
	#elseif defined sql_open
		new 
			escape_ip_addr[16],
			escape_reason[MAX_CHATBUBBLE_LENGTH];

		sql_escape_string(g_sql_connect, g_player_info[targetid][player_ip_address], escape_ip_addr);
		sql_escape_string(g_sql_connect, reason, escape_reason);
		if (reason[0] == '\0' && is_block_ip == 0)
		{
			static const query[] = "INSERT INTO "TABLE_USERS_BAN" \
			(`user_id`, `ip_address`, `adm_id`, `date_banned`, `date_unban`) VALUES \
			('%d', INET_ATON('%s'), '%d', NOW(), NOW() + INTERVAL %d DAY)";
			new fmt_query[sizeof(query) - 2 + 16 + 11 * 3];
			format(
				fmt_query,
				sizeof(fmt_query), 
				query, 
				g_player_info[targetid][player_account_id],
				escape_ip_addr,
				g_player_info[playerid][player_account_id],
				count_days
			);
			sql_query(
				g_sql_connect, 
				fmt_query,
				.flag = QUERY_THREADED,
				.callback = "", 
				.format = ""
			);
		}
		else if (reason[0] != '\0' && is_block_ip == 1)
		{
			static const query[] = "INSERT INTO "TABLE_USERS_BAN" \
			(`user_id`, `ip_address`, `adm_id`, `date_banned`, `date_unban`, `is_block_ip`, `reason`) VALUES \
			('%d', INET_ATON('%s'), '%d', NOW(), NOW() + INTERVAL %d DAY, '%d', '%s')";
			new fmt_query[sizeof(query) - 2 * 6 + 11 * 4 + 64 + 16 + MAX_CHATBUBBLE_LENGTH];
			format(
				fmt_query,
				sizeof(fmt_query), 
				query, 
				g_player_info[targetid][player_account_id],
				escape_ip_addr,
				g_player_info[playerid][player_account_id],
				count_days,
				is_block_ip,
				escape_reason
			);
			sql_query(
				g_sql_connect, 
				fmt_query,
				.flag = QUERY_THREADED,
				.callback = "", 
				.format = ""
			);
		}
		else if (reason[0] != '\0')
		{
			static const query[] = "INSERT INTO "TABLE_USERS_BAN" \
			(`user_id`, `ip_address`, `adm_id`, `date_banned`, `date_unban`, `reason`) VALUES \
			('%d', INET_ATON('%s'), '%d', NOW(), NOW() + INTERVAL %d DAY, '%s')";

			new fmt_query[sizeof(query) - 2 * 6 + 11 * 4 + 64 + 16 + MAX_CHATBUBBLE_LENGTH];
			format(
				fmt_query,
				sizeof(fmt_query), 
				query, 
				g_player_info[targetid][player_account_id],
				escape_ip_addr,
				g_player_info[playerid][player_account_id],
				count_days,
				escape_reason
			);
			sql_query(
				g_sql_connect, 
				fmt_query,
				.flag = QUERY_THREADED,
				.callback = "", 
				.format = ""
			);
		}
		else if (is_block_ip == 1)
		{
			static const query[] = "INSERT INTO "TABLE_USERS_BAN" \
			(`user_id`, `ip_address`, `adm_id`, `date_banned`, `date_unban`, `is_block_ip`) VALUES \
			('%d', INET_ATON('%s'), '%d', NOW(), NOW() + INTERVAL %d DAY, '%d')";
			new fmt_query[sizeof(query) - 2 + 16 + 11 * 3];

			format(
				fmt_query,
				sizeof(fmt_query), 
				query, 
				g_player_info[targetid][player_account_id],
				escape_ip_addr,
				g_player_info[playerid][player_account_id],
				count_days,
				is_block_ip
			);
			sql_query(
				g_sql_connect, 
				fmt_query,
				.flag = QUERY_THREADED,
				.callback = "", 
				.format = ""
			);
		}
	#endif

	static const messsage[] = "Администратор %s[%d] забанил игрока %s[%d]";
	new fmt_message[sizeof(messsage) - 4 * 4 + 11 * 4 + (MAX_PLAYER_NAME + 1) * 4 + MAX_CHATBUBBLE_LENGTH];
	format(
		fmt_message, 
		sizeof(fmt_message), 
		messsage, 
		g_player_info[playerid][player_name],
		playerid,
		g_player_info[targetid][player_name],
		targetid
	);

	if (reason[0] != '\0')
	{
		format(
			fmt_message, 
			sizeof(fmt_message), 
			"%s. Причина: %s", 
			fmt_message,
			reason
		);		
	}
	SendClientMessageToAll(0xFF0000FF, fmt_message);

	if (is_block_ip)
	{
		BlockIpAddress(g_player_info[targetid][player_ip_address], 86400 * 1000 * count_days);
	}
	Kick(targetid);
	return 1;
}