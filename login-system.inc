#if defined login_system_included
	#endinput
#endif
#define login_system_included

/*
	gloval vars 
*/




#if defined mysql_included 
	static Cache: g_player_load_account_cache[MAX_PLAYERS] = { Cache:-1, Cache:-1, Cache:-1, ... };
#elseif defined sql_open
	static Result: g_player_load_account_cache[MAX_PLAYERS] = { Result:-1, Result:-1, Result:-1, ... };
#endif

static g_player_count_login_attempts[MAX_PLAYERS] = { 0, 0, 0, ... };

#if !defined _mdialog_included && !defined _TDW_DIALOG_INC_

	/*
		hook OnDialogResponse
	*/

	public OnDialogResponse(playerid, dialogid, response, listitem, inputtext[])
	{
		if (dialogid == DIALOG_REGISTRATION)
		{
			new ret = DialogHandlerRegistration(playerid, response, inputtext);
			return ret;
		}
		else if (dialogid == DIALOG_AUTHORIZATION)
		{
			new ret = DialogHandlerAutorization(playerid, response, inputtext);
			return ret;
		}
		#if defined login_system_OnDialogResponse
			return login_system_OnDialogResponse(playerid, dialogid, response, listitem, inputtext);
		#else
			return 1;
		#endif
	}
	#if defined _ALS_OnDialogResponse
		#undef OnDialogResponse
	#else
		#define _ALS_OnDialogResponse
	#endif

	#define OnDialogResponse login_system_OnDialogResponse
	#if defined login_system_OnDialogResponse
		forward login_system_OnDialogResponse(playerid, dialogid, response, listitem, inputtext[]);
	#endif

	static stock ShowPlayerDialogAuthorization(const playerid)
	{
		static buffer[256]; // you need to put the cock on the memory, it's not so important. it's better to chase CPU time
		GetDialogTextAuthorization(playerid, buffer);
		ShowPlayerDialog(
			playerid, 
			DIALOG_AUTHORIZATION, 
			DIALOG_STYLE_INPUT, 
			"{00bfff}Авторизация", 
			buffer, 
			"Ок", 
			"Выход"
		);
		return 1;
	}
#endif

/*
	hook callback
*/

public OnPlayerDisconnect(playerid, reason)
{
	g_player_count_login_attempts[playerid] = 0;

	#if defined mysql_included
		new Cache: cache_id = g_player_load_account_cache[playerid];
		if (cache_is_valid(cache_id))
		{
			cache_delete(cache_id);
			g_player_load_account_cache[playerid] = Cache:-1;
		}
	#elseif defined sql_open
		new Result: cache_id = g_player_load_account_cache[playerid];
		if (cache_id != Result:-1)
		{
			sql_free_result(cache_id);
			g_player_load_account_cache[playerid] = Result:-1;
		}
	#endif

	#if defined login_system_OnPlayerDisconnect
		return login_system_OnPlayerDisconnect(playerid, reason);
	#else
		return 1;
	#endif
}
#if defined _ALS_OnPlayerDisconnect
	#undef OnPlayerDisconnect
#else
	#define _ALS_OnPlayerDisconnect
#endif

#define OnPlayerDisconnect login_system_OnPlayerDisconnect
#if defined login_system_OnPlayerDisconnect
	forward login_system_OnPlayerDisconnect(playerid, reason);
#endif

/*
	MySQL callbacks
*/

#if defined mysql_included
	forward OnPlayerAccountLoad(const playerid);
	public OnPlayerAccountLoad(const playerid)
#elseif defined sql_open
	forward OnPlayerAccountLoad(const Result: res, const playerid);
	public OnPlayerAccountLoad(const Result: res, const playerid)
#endif
{
	#if defined mysql_included
		new rows = cache_num_rows();
		if (rows == 0)
		{
			#if defined _TDW_DIALOG_INC_
				Dialog_Show(playerid, "DialogRegistrations");
			#elseif defined _mdialog_included
				Dialog_Show(playerid, Dialog:DialogRegistrations);
			#else
				static buffer[256]; // you need to put the cock on the memory, it's not so important. it's better to chase CPU time
				GetDialogTextRegistrations(playerid, buffer);
				ShowPlayerDialog(
					playerid, 
					DIALOG_REGISTRATION, 
					DIALOG_STYLE_INPUT, 
					"{00bfff}Регистрация", 
					buffer, 
					"Ок", 
					"Выход"
				);
			#endif
		}
		else 
		{
			#if defined _TDW_DIALOG_INC_
				Dialog_Show(playerid, "DialogAuthorization");
			#elseif defined _mdialog_included
				Dialog_Show(playerid, Dialog:DialogAuthorization);
			#else 
				ShowPlayerDialogAuthorization(playerid);
			#endif
			g_player_load_account_cache[playerid] = cache_save();

			cache_get_value_name(
				0, 
				"user_password", 
				g_player_info[playerid][hash_password],
				MAX_HASH_PASSWORD
			);
			cache_get_value_name(
				0, 
				"user_salt", 
				g_player_info[playerid][salt_password],
				MAX_SALT_PASSWORD
			);
			cache_get_value_name_int(0, "user_id", g_player_info[playerid][player_account_id]);
			cache_unset_active();
		}
	#elseif defined sql_open
		new rows = sql_num_rows(res);
		if (rows == 0)
		{
			#if defined _TDW_DIALOG_INC_
				Dialog_Show(playerid, "DialogRegistrations");
			#elseif defined _mdialog_included
				Dialog_Show(playerid, Dialog:DialogRegistrations);
			#else
				static buffer[256]; // you need to put the cock on the memory, it's not so important. it's better to chase CPU time
				GetDialogTextRegistrations(playerid, buffer);
				ShowPlayerDialog(
					playerid, 
					DIALOG_REGISTRATION, 
					DIALOG_STYLE_INPUT, 
					"{00bfff}Регистрация", 
					buffer, 
					"Ок", 
					"Выход"
				);
			#endif
		}
		else 
		{
			#if defined _TDW_DIALOG_INC_
				Dialog_Show(playerid, "DialogAuthorization");
			#elseif defined _mdialog_included
				Dialog_Show(playerid, Dialog:DialogAuthorization);
			#else 
				ShowPlayerDialogAuthorization(playerid);
			#endif
			g_player_load_account_cache[playerid] = res;
			g_player_info[playerid][player_account_id] = sql_get_field_assoc_int(res, "user_id");
			sql_get_field_assoc(res, "user_password", g_player_info[playerid][hash_password], MAX_HASH_PASSWORD);
			sql_get_field_assoc(res, "user_salt", g_player_info[playerid][salt_password], MAX_HASH_PASSWORD);
			sql_store_result(res);
		}
		
	#endif
}

#if defined mysql_included
	forward OnPlayerRegistration(const playerid);
	public OnPlayerRegistration(const playerid)
#elseif defined sql_open
	forward OnPlayerRegistration(const Result: res, const playerid);
	public OnPlayerRegistration(const Result: res, const playerid)
#endif
{
	new pid = 0;
	#if defined mysql_included
		pid = cache_insert_id();
	#elseif defined sql_open
		pid = sql_insert_id(res);
	#endif
	g_player_info[playerid][player_account_id] = pid;

	static const message[] = "Вы зарегистрировали аккаунт с идом %d";
	new fmt_message[sizeof(message) - 2 + 11];
	format(fmt_message, sizeof(fmt_message), message, pid);
	SendClientMessage(playerid, -1, fmt_message);
	return 1;
}


/*
	function
*/

static stock GetDialogTextRegistrations(const playerid, dialog_text[], const size = sizeof(dialog_text))
{
	static const dlg_text[] = "{ffffff}Приветствуем на нашем сервере. Аккаунт с ником %s не зарегистрирован на сервере!\n\
	Предлагаем Вам зарегестрировать аккаунт, для это введите пароль в поле ниже:";

	format(dialog_text, size, dlg_text, g_player_info[playerid][player_name]);
	return 1;
}

static stock GetDialogTextAuthorization(const playerid, dialog_text[], const size = sizeof(dialog_text))
{
	static const dlg_text[] = "{ffffff}Приветствуем на нашем сервере. Аккаунт с ником %s зарегистрирован на сервере!\n\
	Предлагаем Вам ввести пароль от аккаунта, для это введите пароль в поле ниже:";
	
	format(dialog_text, size, dlg_text, g_player_info[playerid][player_name]);
	return 1;
}

/*
	Dialog handler
*/

static stock DialogHandlerRegistration(const playerid, const response, const inputtext[])
{
	if (!response)
	{
		SendClientMessage(playerid, -1, "Вы отказались от авторизации за что были кикнуты!");
		Kick(playerid);
		return 0;
	}
	new 
		salt[MAX_SALT_PASSWORD],
		hash[MAX_HASH_PASSWORD];

	utils_GenerateRandomString(salt, MAX_SALT_PASSWORD);

	#if defined mysql_included
		SHA256_PassHash(inputtext, salt, hash, sizeof(hash));
		static const query[] = "INSERT INTO "TABLE_USERS" (`user_name`, `user_password`, `user_salt`) \
		VALUES ('%s', '%s', '%s')";
		new fmt_query[sizeof(query) + MAX_PLAYER_NAME + 1 + MAX_HASH_PASSWORD + MAX_SALT_PASSWORD];
		mysql_format(
			g_sql_connect,
			fmt_query,
			sizeof(fmt_query),
			query, 
			g_player_info[playerid][player_name],
			hash,
			salt
		);
		mysql_tquery(
			g_sql_connect, 
			fmt_query, 
			"OnPlayerRegistration", 
			"d", 
			playerid
		);	
	#elseif defined sql_open
		new	
			escape_inputtext[256],
			escape_nickname[MAX_PLAYER_NAME + 1];

		sql_escape_string(g_sql_connect, inputtext, escape_inputtext);
		sql_escape_string(g_sql_connect, g_player_info[playerid][player_name], escape_nickname);

		SHA256_PassHash(escape_inputtext, salt, hash, sizeof(hash));

		static const query[] = "INSERT INTO "TABLE_USERS" (`user_name`, `user_password`, `user_salt`) \
		VALUES ('%s', '%s', '%s')";
		new fmt_query[sizeof(query) + MAX_PLAYER_NAME + 1 + MAX_HASH_PASSWORD + MAX_SALT_PASSWORD];
		format(
			fmt_query,
			sizeof(fmt_query),
			query, 
			escape_nickname,
			hash,
			salt
		);
		sql_query(
			g_sql_connect,
			fmt_query, 
			QUERY_THREADED, 
			"OnPlayerRegistration", 
			"rd",
			playerid
		);
	#endif
	g_player_info[playerid][hash_password][0] = '\0';
	g_player_info[playerid][salt_password][0] = '\0';


	ShowPlayerDialogAuthorization(playerid);
	return 1;
}


static stock DialogHandlerAutorization(const playerid, const response, const inputtext[])
{
	if (!response)
	{
		SendClientMessage(playerid, -1, "Вы отказались от авторизации за что были кикнуты!");
		Kick(playerid);
		return 0;
	}
	if (g_player_count_login_attempts[playerid] == MAX_ATTEMPTS_INPUT_PASSWORD - 1)
	{
		SendClientMessage(playerid, -1, "Вы потратили попытки на авторизацию");
		Kick(playerid);
		return 0;
	}
	if (!utils_ComparePasswordHash(playerid, inputtext))
	{
		g_player_count_login_attempts[playerid]++;
		static const message[] = "Пароль введен не правильно. У Вас осталось %d попыток на авторизацию!";
		new fmt_message[sizeof(message) - 2 + 11];
		format(
			fmt_message, 
			sizeof(fmt_message),
			message, 
			MAX_ATTEMPTS_INPUT_PASSWORD - g_player_count_login_attempts[playerid]
		);
		SendClientMessage(playerid, -1, fmt_message);
		ShowPlayerDialogAuthorization(playerid);
		return 0;
	}

	LoadAccountData(playerid);
	
	static const query[] = "SELECT \
		ub.user_id, \
		ua.user_name, \
		ub.date_unban, \
		ub.date_banned, \
		ub.reason, \
		INET_NTOA(ub.ip_address) as ip_address, \
		timestampdiff(DAY, NOW(), date_unban) + 1 as days_unban, \
		timestampdiff(SECOND, NOW(), date_unban) * 1000 AS ms_unban, \
		uc.user_name AS adm_user_name \
	FROM user_bans ub \
		LEFT JOIN user_accounts ua \
			ON ua.user_id = ub.user_id \
		LEFT JOIN user_accounts uc \
			ON uc.user_id = ub.adm_id \
	WHERE ub.user_id = '%d' AND (ua.user_id IS NOT NULL OR uc.user_id IS NOT NULL)";
	new fmt_query[sizeof(query) - 2 + 11];

	/*
		non thread for wait load data
	*/

	#if defined mysql_included
		mysql_format(
			g_sql_connect, 
			fmt_query, 
			sizeof(fmt_query),
			query,
			g_player_info[playerid][player_account_id]
		);
		new Cache: cache_id = mysql_query(g_sql_connect, fmt_query, true);
		OnPlayerLoadBans(cache_id, playerid);
	#elseif defined sql_open
		format(
			fmt_query, 
			sizeof(fmt_query),
			query,
			g_player_info[playerid][player_account_id]
		);
		sql_query(
			g_sql_connect, 
			fmt_query, 
			QUERY_NONE, 
			"OnPlayerLoadBans", 
			"rd", 
			playerid
		);
	#endif

	if (block_system_IsPlayerBlock(playerid))
	{
		Kick(playerid);
		return 0;
	}
	SetSpawnInfo(
		playerid,
		NO_TEAM,
		random(311) + 1, 
		-2269.7891,
		2348.6536,
		4.8202,
		207.9640,
		0, 
		0, 
		0, 
		0, 
		0, 
		0
	);
	SpawnPlayer(playerid);
	return 1;
}

stock LoadAccountData(playerid)
{
	#if defined mysql_included
		new Cache: cache_id = g_player_load_account_cache[playerid];

		if (!cache_is_valid(cache_id))
		{
			return 0;
		}

		cache_set_active(cache_id);

		cache_get_value_name_int(0, "user_id", g_player_info[playerid][player_account_id]);
		cache_get_value_name_int(0, "user_adm_level", g_player_info[playerid][level_adm]);

		cache_delete(cache_id);

		g_player_load_account_cache[playerid] = Cache:-1;

	#elseif defined sql_open
		new Result: cache_id = g_player_load_account_cache[playerid]; 
		if (cache_id == Result:-1)
		{
			return 0;
		}
		g_player_info[playerid][player_account_id] = sql_get_field_assoc_int(cache_id, "user_id");
		g_player_info[playerid][level_adm] = sql_get_field_assoc_int(cache_id, "user_adm_level");
		sql_free_result(cache_id);
		g_player_load_account_cache[playerid] = Result:-1;
	#endif
	return 1;
}

#if defined _TDW_DIALOG_INC_
	dtempl DialogRegistrations(playerid)
	{
		static buffer[256]; // you need to put the cock on the memory, it's not so important. it's better to chase CPU time
		GetDialogTextRegistrations(playerid, buffer);
		Dialog_Open(
			playerid, 
			dfunc:DialogRegistrations, 
			DIALOG_STYLE_MSGBOX, 
			"{00bfff}Регистрация", 
			buffer, 
			"Ок", 
			"Выход"
		);
		return 1;
	}
	dialog DialogRegistrations(playerid, response, inputtext[])
	{
		new ret = DialogHandlerRegistration(playerid, response, inputtext);
		return ret;
	}

	dtempl DialogAuthorization(playerid)
	{
		static buffer[256]; // you need to put the cock on the memory, it's not so important. it's better to chase CPU time
		GetDialogTextAuthorization(playerid, buffer);
		Dialog_Open(
			playerid, 
			dfunc:DialogAuthorization, 
			DIALOG_STYLE_MSGBOX, 
			"{00bfff}Авторизация", 
			buffer, 
			"Ок", 
			"Выход"
		);
		return 1;
	}
	dialog DialogAuthorization(playerid, response, inputtext[])
	{
		new ret = DialogHandlerAutorization(playerid, response, inputtext);
		return ret;
	}
#elseif defined _mdialog_included
	DialogCreate:DialogRegistrations(playerid)
	{
		static buffer[256]; // you need to put the cock on the memory, it's not so important. it's better to chase CPU time
		GetDialogTextRegistrations(playerid, buffer);
		Dialog_Open(
			playerid, 
			Dialog:DialogRegistrations, 
			DIALOG_STYLE_MSGBOX,
			"{00bfff}Регистрация", 
			buffer
			"Ок", 
			"Выход"
		);
	}
	DialogResponse:DialogRegistrations(playerid, response, listitem, inputtext[])
	{
		new ret = DialogHandlerAutorization(playerid, response, inputtext);
		return ret;
	}

	DialogCreate:DialogAuthorization(playerid)
	{
		static buffer[256]; // you need to put the cock on the memory, it's not so important. it's better to chase CPU time
		GetDialogTextAuthorization(playerid, buffer);
		Dialog_Open(
			playerid, 
			Dialog:DialogAuthorization, 
			DIALOG_STYLE_MSGBOX,
			"{00bfff}Авторизация", 
			buffer
			"Ок", 
			"Выход"
		);
		return 1;
	}
	DialogResponse:DialogAuthorization(playerid, response, listitem, inputtext[])
	{
		new ret = DialogHandlerAutorization(playerid, response, inputtext);
		return ret;
	}
#endif