enum
{
	hcs__PLAYER_REQUEST_CLASS,
	hcs__PLAYER_SPAWN,
	hcs__PLAYER_DEATH,
}

static
	bool:hcs__buttom_status[MAX_PLAYERS char] = { true, true, ...},
	hcs__player_status[MAX_PLAYERS char];

/*
================================================================================
	========================================================================
================================================================================
*/

stock ToggleSkinSelectionButtons(playerid, bool:toggle)
{
	switch(GetPlayerState(playerid))
	{
		case PLAYER_STATE_NONE, PLAYER_STATE_WASTED, PLAYER_STATE_SPECTATING:
		{
			if(toggle == true)
			{
				ForceClassSelection(playerid);
				TogglePlayerSpectating(playerid, true);
				TogglePlayerSpectating(playerid, false);
			}
			else
				TogglePlayerSpectating(playerid, true);
		}
	}
	hcs__buttom_status{playerid} = toggle;
	return 1;
}

/*
================================================================================
	========================================================================
================================================================================
*/

stock hidesclass_SpawnPlayer(playerid)
{
	if(hcs__buttom_status{playerid} == false && hcs__player_status{playerid} != hcs__PLAYER_SPAWN)
	{
		switch(GetPlayerState(playerid))
		{
			case PLAYER_STATE_WASTED, PLAYER_STATE_SPECTATING:
			{
				if(!IsPlayerConnected(playerid))// Р”Р»СЏ СЃРѕС…СЂР°РЅРµРЅРёСЏ Р»РѕРіРёРєРё СЂР°Р±РѕС‚С‹ С„СѓРЅРєС†РёРё SpawnPlayer
					return 0;
				hcs__player_status{playerid} = hcs__PLAYER_SPAWN;
				TogglePlayerSpectating(playerid, false);
				return 1;
			}
		}
	}
	return SpawnPlayer(playerid);
}
#if defined _ALS_SpawnPlayer
	#undef SpawnPlayer
#else
	#define _ALS_SpawnPlayer
#endif
#define SpawnPlayer hidesclass_SpawnPlayer

/*
================================================================================
	========================================================================
================================================================================
*/

public OnPlayerDeath(playerid, killerid, reason)
{
	hcs__player_status{playerid} = hcs__PLAYER_DEATH;
	#if defined hideclass_OnPlayerDeath
		return hideclass_OnPlayerDeath(playerid, killerid, reason);
	#else
		return 1;
	#endif
}
#if defined _ALS_OnPlayerDeath
	#undef OnPlayerDeath
#else
	#define _ALS_OnPlayerDeath
#endif

#define OnPlayerDeath hideclass_OnPlayerDeath
#if defined hideclass_OnPlayerDeath
	forward hideclass_OnPlayerDeath(playerid, killerid, reason);
#endif

/*
================================================================================
	========================================================================
================================================================================
*/

public OnPlayerRequestClass(playerid, classid)
{
	switch(hcs__player_status{playerid})
	{
		case hcs__PLAYER_SPAWN:
		{
			SetSpawnInfo(playerid, NO_TEAM, 0, 0.0, 0.0, 0.0, 0.0, 0, 0, 0, 0, 0, 0);
			SpawnPlayer(playerid);
			return 1;
		}
		case hcs__PLAYER_DEATH:
		{
			if(hcs__buttom_status{playerid} == false)
			{
				SetSpawnInfo(playerid, NO_TEAM, 0, 0.0, 0.0, 0.0, 0.0, 0, 0, 0, 0, 0, 0);
				TogglePlayerSpectating(playerid, true);
				SetTimerEx("@_DisablePlayerSpectatingRClass", 500, 0, "i", playerid);
				return 1;
			}
		}
	}

	#if defined hidesclass_OnPlayerRequestClass
		return hidesclass_OnPlayerRequestClass(playerid, classid);
	#else
		return 1;
	#endif
}
#if defined _ALS_OnPlayerRequestClass
	#undef OnPlayerRequestClass
#else
	#define _ALS_OnPlayerRequestClass
#endif

#define OnPlayerRequestClass hidesclass_OnPlayerRequestClass
#if defined hidesclass_OnPlayerRequestClass
	forward hidesclass_OnPlayerRequestClass(playerid, classid);
#endif

/*
================================================================================
	========================================================================
================================================================================
*/

public OnPlayerConnect(playerid)
{
	new reason_result = 1;

	hcs__buttom_status{playerid} = false;

	#if defined hideclass_OnPlayerConnect
		reason_result = hideclass_OnPlayerConnect(playerid);
	#endif

	hcs__player_status{playerid} = hcs__PLAYER_REQUEST_CLASS;

	return reason_result;
}
#if defined _ALS_OnPlayerConnect
	#undef OnPlayerConnect
#else
	#define _ALS_OnPlayerConnect
#endif

#define OnPlayerConnect hideclass_OnPlayerConnect
#if defined hideclass_OnPlayerConnect
	forward hideclass_OnPlayerConnect(playerid);
#endif

/*
================================================================================
	========================================================================
================================================================================
*/

public OnPlayerRequestSpawn(playerid)
{
	#if defined hodesclass_OnPlayerRequestSpawn
		return hodesclass_OnPlayerRequestSpawn(playerid);
	#else
		return 1;
	#endif
}
#if defined _ALS_OnPlayerRequestSpawn
	#undef OnPlayerRequestSpawn
#else
	#define _ALS_OnPlayerRequestSpawn
#endif

#define OnPlayerRequestSpawn hodesclass_OnPlayerRequestSpawn
#if defined hodesclass_OnPlayerRequestSpawn
	forward hodesclass_OnPlayerRequestSpawn(playerid);
#endif

/*
================================================================================
	========================================================================
================================================================================
*/

forward SetPlayerCameraPosForReqClass(playerid);
public SetPlayerCameraPosForReqClass(playerid)
{
	SetPlayerCameraPos(playerid, 1093.0, -2036.0, 90.0);
	SetPlayerCameraLookAt(playerid, 384.0, -1557.0, 20.0, CAMERA_CUT);
}

/*
================================================================================
	========================================================================
================================================================================
*/

@_DisablePlayerSpectatingRClass(playerid);
@_DisablePlayerSpectatingRClass(playerid)
	return TogglePlayerSpectating(playerid, false);