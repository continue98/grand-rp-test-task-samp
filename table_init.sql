create table if not exists user_accounts
(
	user_id int auto_increment
		primary key,
	user_name varchar(24) default null,
	user_adm_level int default null,
	user_password varchar(255) default null,
	user_salt varchar(64) default null
)
;

create table if not exists user_bans
(
	id int auto_increment
		primary key,
	user_id int default null,
	ip_address bigint default null,
	reason varchar(145) default null,
	adm_id int default null,
	date_banned datetime default null,
	date_unban datetime default null,
	is_block_ip tinyint default null
)
;

create definer = `continue`@`%` event UnBanPlayers_schedule on schedule
	every '1' DAY
	starts '2021-03-26 05:00:00'
	enable
	comment 'Unban players'
	do
	DELETE FROM user_bans WHERE NOW() >= date_unban
;

