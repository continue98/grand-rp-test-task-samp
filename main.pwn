/*
	Test task Grand RP (grand-rp.su)
*/

#include <a_samp>
#include <sql> // or a_mysql
#include <dc_kickfix>
#include <crashdetect>
#include <Pawn.CMD>
#include <sscanf2>
#include <skip_samp_select_skin>

main()
{

}

#if !defined mysql_included && !defined sql_open
	#error "Include sql.inc or a_mysql.inc"
#endif

#if !defined MAX_HASH_PASSWORD
	const MAX_HASH_PASSWORD = 256;
#endif 

#if !defined MAX_SALT_PASSWORD
	const MAX_SALT_PASSWORD = 32;
#endif

#if !defined MAX_ATTEMPTS_INPUT_PASSWORD
	const MAX_ATTEMPTS_INPUT_PASSWORD = 5;
#endif

enum e_player_info
{
	player_account_id,
	player_ip_address[16],
	player_name[MAX_PLAYER_NAME + 1],
	hash_password[MAX_HASH_PASSWORD],
	salt_password[MAX_SALT_PASSWORD],
	level_adm
}
new g_player_info[MAX_PLAYERS][e_player_info];

#if !defined TABLE_USERS
	#define TABLE_USERS "user_accounts"
#endif 

#if !defined TABLE_USERS_BAN
	#define TABLE_USERS_BAN "user_bans"
#endif 

forward bool: block_system_IsPlayerBlock(const playerid);

#if defined mysql_included
	new MySQL: g_sql_connect;
#elseif defined sql_open
	new SQL: g_sql_connect;
#endif

#if !defined _mdialog_included && !defined _TDW_DIALOG_INC_
	enum
	{
		DIALOG_NULL = 0,
		DIALOG_REGISTRATION,
		DIALOG_AUTHORIZATION,
		DIALOG_BAN_INFO
	};
#endif

#include <utils>
#include <commands>
#include <login-system>
#include <block_system>

public OnGameModeInit()
{
	g_sql_connect = utils_ConnectSQL("host", "user", "password", "database");
	block_system_SetMySQLHandle(g_sql_connect);
	block_system_LoadIpAddressBan();
	return 1;
}
public OnPlayerConnect(playerid)
{
	UsePlayerPedAnims();
	ToggleSkinSelectionButtons(playerid, false);
	SetTimerEx("SetPlayerCameraPosForReqClass", 100, 0, "i", playerid);

	GetPlayerName(playerid, g_player_info[playerid][player_name], MAX_PLAYER_NAME + 1);
	GetPlayerIp(playerid, g_player_info[playerid][player_ip_address], 16);

	#if defined mysql_included
		static const query[] = "SELECT * FROM "TABLE_USERS" WHERE `user_name` = '%e'";
		new fmt_query[sizeof(query) - 2 + MAX_PLAYER_NAME + 1];
		mysql_format(
			g_sql_connect, 
			fmt_query,
			sizeof(fmt_query),
			query,
			g_player_info[playerid][player_name]
		);
		mysql_tquery(
			g_sql_connect,
			fmt_query,
			"OnPlayerAccountLoad",
			"d",
			playerid
		);
	#elseif defined sql_open
		new escape_nickname[MAX_PLAYER_NAME];
		sql_escape_string(
			g_sql_connect, 
			g_player_info[playerid][player_name],
			escape_nickname 
		);
		static const query[] = "SELECT * FROM "TABLE_USERS" WHERE `user_name` = '%s'";
		new fmt_query[sizeof(query) - 2 + MAX_PLAYER_NAME + 1];
		format(
			fmt_query, 
			sizeof(fmt_query), 
			query,
			escape_nickname
		);
		sql_query(
			g_sql_connect, 
			fmt_query, 
			QUERY_THREADED, 
			"OnPlayerAccountLoad", 
			"rd",
			playerid
		);
	#endif 
	return 1;
}

