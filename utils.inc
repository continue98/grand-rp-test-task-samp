#if defined utils_included
	#endinput
#endif
#define utils_included

/*
	definitions
*/

#if !defined DEFAULT_ALPHABET
	#define DEFAULT_ALPHABET "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
#endif

#if defined mysql_included
	stock MySQL:utils_ConnectSQL(const host[], const user[], const password[], const database[])
#elseif defined sql_open
	stock SQL:utils_ConnectSQL(const host[], const user[], const password[], const database[])
#endif
{
	#if defined mysql_included
		new MySQLOpt:options = mysql_init_options();
		mysql_set_option(options, AUTO_RECONNECT, true);
		mysql_set_option(options, POOL_SIZE, 4);
		return mysql_connect(host, user, password, database, .option_id = options);
	#elseif defined sql_open
		return sql_connect(SQL_HANDLER_MYSQL, host, user, password, database, .port = 3306);
	#endif
}



stock utils_GenerateRandomString(result_str[], length, const size = sizeof(result_str), const alphabet[] = DEFAULT_ALPHABET, const alphabet_size = sizeof(alphabet))
{
	result_str[0] = '\0';

	if (length >= size) 
	{
		length = size - 1;
	}

	if (length < 1) 
	{
		return 0;
	}

	for (new i = 0; i < length; i++) 
	{
		result_str[i] = alphabet[random(alphabet_size - 1)];
	}

	return 1;
}


stock bool: utils_ComparePasswordHash(const playerid, const input_password[])
{
	new 
		hash[MAX_HASH_PASSWORD];
		
	SHA256_PassHash(input_password, g_player_info[playerid][salt_password], hash, sizeof(hash));

	if (strcmp(g_player_info[playerid][hash_password], hash) == 0)
	{
		return true;
	}
	return false;
}

stock utils_GetDeclensionWord(result[], const size = sizeof(result), num, const word_1[], const word_2[], const word_3[])
{
	num %= 100;
	if (num > 19) 
	{
		num %= 10;
	}

	result[0] = '\0';
	
	switch (num) 
	{
		case 1: 
		{
			strcat(result, word_1, size);
		}
		case 2..4: 
		{
			strcat(result, word_2, size);
		}
		default: 
		{
			strcat(result, word_3, size);
		}
	}
	return 1;
}